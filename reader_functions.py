def parse_crystal_coord_atom(line):
    """Parses one atomic line!"""
    try:
        split_line = line.strip().split()
        type = split_line[1]
        label = split_line[2]
        u, v, w = float(split_line[3]), float(split_line[4]), float(split_line[5])
        return type, label, u, v, w
    except:
        ValueError("Couldn't read line: \n{0}".format(line))


def parse_casep_atom(line):
    """Parses one atomic line!"""
    try:
        split_line = line.strip().split()
        type = split_line[1]
        label = split_line[1] + split_line[2]
        u, v, w = float(split_line[3]), float(split_line[4]), float(split_line[5])
        return type, label, u, v, w
    except:
        ValueError("Couldn't read line: \n{0}".format(line))


def parse_casep_atom_chemical_information(target_atom, line):
    try:
        split_line = line.strip().split()
        target_atom.iso = float(split_line[3])
        target_atom.aniso = float(split_line[4])
        target_atom.asym = float(split_line[5])
        target_atom.cq = float(split_line[6])
        target_atom.eta = float(split_line[7])
    except:
        ValueError("Couldn't read line: \n{0}".format(line))
