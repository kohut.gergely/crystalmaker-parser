# HELPER FUNCTIONS
from math import cos, sin, radians, sqrt

def distance_visible_coord_atom(row):
    split_line = row.strip().split()
    distance = split_line[-1]
    return float(distance)


def distance(x1, y1, z1, x2, y2, z2):
    return sqrt(((x1 - x2) ** 2) + ((y1 - y2) ** 2) + ((z1 - z2) ** 2))


def generate_combinations(u, v, w):
    combinations = []
    for item1 in (0, -1, 1):
        for item2 in (0, -1, 1):
            for item3 in (0, -1, 1):
                combinations.append((item1 + u, item2 + v, item3 + w))
    return combinations


def convert_fract_to_cart(unit_cell, u, v, w, cell_volume):
    x = unit_cell.a * u + unit_cell.b * v * cos(
        radians(unit_cell.gamma)) + unit_cell.c * w * cos(
        radians(unit_cell.beta))
    y = unit_cell.b * v * sin(radians(unit_cell.gamma)) + unit_cell.c * w \
        * (cos(radians(unit_cell.alpha)) - cos(radians(unit_cell.beta)) * cos(
        radians(unit_cell.gamma))) / sin(
        radians(unit_cell.gamma))
    z = cell_volume / (unit_cell.a * unit_cell.b * sin(radians(unit_cell.gamma))) * w

    return x, y, z


def cell_volume(unit_cell):
    return unit_cell.a * unit_cell.b * unit_cell.c \
           * sqrt(
        1 - cos(radians(unit_cell.alpha)) ** 2 - cos(radians(unit_cell.beta)) ** 2 - cos(
            radians(unit_cell.gamma)) ** 2 + \
        2 * cos(radians(unit_cell.alpha)) * cos(radians(unit_cell.beta)) * cos(
            radians(unit_cell.gamma)))
