from collections import namedtuple
import copy

from .structure_functions import *

# namedtuple structure for the unit cell parameters!
UnitCell = namedtuple('UnitCell', 'a b c alpha beta gamma')
# namedtuple structure for the bond specifications!
BondSpec = namedtuple('BondSpec', 'spec_name type1 type2 lower_bound upper_bound')
# namedtuple structure for one coordinating atom! It is necessary to store the related distances as well!
CoordAtom = namedtuple('CoordAtom', 'atom distance spec_name')

PrintableParam = namedtuple('PrintableParam', 'label filename type iso cq eta coord grp grp_rep u v w x y z aniso asym')


class CastepAtom:

    def __init__(self, filename, type, label, u, v, w, iso=None, aniso=None, asym=None, cq=None, eta=None,
                 unit_cell=None, cell_volume=None):
        self.unit_cell = unit_cell
        self.filename = filename.strip('.castep')
        self.type = str(type.strip())
        self.label = label
        self.u = u
        self.v = v
        self.w = w
        self.coordinating_atoms = []
        self.iso = iso
        self.aniso = aniso
        self.asym = asym
        self.cq = cq
        self.eta = eta
        self.group = None
        self.group_repr = None
        self.cell_volume = cell_volume
        self.x, self.y, self.z = convert_fract_to_cart(self.unit_cell, self.u, self.v, self.w, self.cell_volume)
        self._combinations = None
        self.is_printable = PrintableParam(True, True, False, True, True, True, True, True, True,
                                           False, False, False, False, False, False, False, False)
        self._printable_type = (
        str, str, str, float, float, float, int, int, str, float, float, float, float, float, float, float, float)

    def __repr__(self):
        return '{:5}'.format(self.label)

    @property
    def coordination_number(self):
        if len(self.coordinating_atoms):
            return len(self.coordinating_atoms)
        else:
            return None

    @property
    def combinations(self):
        if self._combinations is None:
            combs = generate_combinations(self.u, self.v, self.w)
            self._combinations = [convert_fract_to_cart(self.unit_cell, u, v, w, self.cell_volume) for u, v, w in combs]
        return self._combinations

    @property
    def printable_params(self):
        return (self.label, self.filename, self.type, self.iso, self.cq, self.eta, self.coordination_number, self.group,
                self.group_repr, self.u, self.v, self.w, self.x, self.y, self.z, self.aniso, self.asym)

    @property
    def repr(self):

        list_to_write = []
        for item, is_printable in zip(self.printable_params, self.is_printable):
            if is_printable:
                if item is not None:
                    list_to_write.append(item)
        return self._format().format(*list_to_write)

    def _format(self, header=False):

        pairs = zip(self.printable_params, self.is_printable,self._printable_type)
        fmt = ''
        for param, is_printable, printable_type in pairs:
            if is_printable:
                if header:

                    if printable_type == float:
                        fmt += '{:<16s}'
                    elif printable_type == int:
                        fmt += '{:<8s}'
                    elif printable_type == str:
                        fmt += '{:40s}'
                    else:
                        fmt += '{:40s}'

                else:
                    if param is None:
                        if printable_type == float:
                            fmt += '{:16s}'.format('None')
                        elif printable_type == int:
                            fmt += '{:8s}'.format('None')
                        elif printable_type == str:
                            fmt += '{:40s}'.format('None')
                    else:
                        if isinstance(param, float):
                            fmt += '{:<16.6f}'
                        elif isinstance(param, int):
                            fmt += '{:<8d}'
                        elif isinstance(param, str):
                            fmt += '{:40s}'
        return fmt


class CrystalData:
    """Container which stores all the relevant information (box lengths, angles, bond specifications, atoms) of
    a CrystalMaker coordination file!"""

    def __init__(self, filename, unit_cell, struct_id, bond_spec=None, atoms=None, cell_volume=None,
                 print_header=False):
        self.filename = filename
        self.unit_cell = unit_cell
        if bond_spec is None:
            self._bond_specs = []
        else:
            self._bond_specs = bond_spec

        if atoms is None:
            self.atoms = []
        else:
            self.atoms = atoms
        self.print_header = print_header
        self._cell_volume = cell_volume
        self.struct_id = struct_id

    @property
    def cell_volume(self):
        if self._cell_volume is None:
            self._cell_volume = self.unit_cell.a * self.unit_cell.b * self.unit_cell.c \
                                * sqrt(
                1 - cos(radians(self.unit_cell.alpha)) ** 2 - cos(radians(self.unit_cell.beta)) ** 2 - cos(
                    radians(self.unit_cell.gamma)) ** 2 + \
                2 * cos(radians(self.unit_cell.alpha)) * cos(radians(self.unit_cell.beta)) * cos(
                    radians(self.unit_cell.gamma)))
        return self._cell_volume

    def __len__(self):
        return len(self.atoms)

    def __repr__(self):
        return '{0} with {1} atoms'.format(self.filename, len(self))

    def __getitem__(self, item):
        return self.atoms[item]

    def calculate_coordination(self):

        for atom in self.atoms:
            atom.coordinating_atoms = []
        for index, atom1 in enumerate(self.atoms):
            for atom2 in self.atoms[index + 1:]:
                min_distance = min(
                    [distance(atom1.x, atom1.y, atom1.z, item[0], item[1], item[2]) for item in atom2.combinations])
                if self._bond_specs:
                    for bond_spec in self._bond_specs:
                        if ((atom1.type, atom2.type) == (bond_spec.type1, bond_spec.type2) or (
                                atom1.type, atom2.type) == (bond_spec.type2,
                                                            bond_spec.type1)) and min_distance < bond_spec.upper_bound and min_distance > bond_spec.lower_bound:
                            atom1.coordinating_atoms.append(CoordAtom(atom2, min_distance, bond_spec.spec_name))
                            atom2.coordinating_atoms.append(CoordAtom(atom1, min_distance, bond_spec.spec_name))
                else:
                    raise ValueError(
                        'Before coordination calculation the bond specifications need to be added using "add_bond_specification" method!')

    def get_atom(self, label):
        """Returns the CrystalAtom object of the given atomic label!"""
        for atom in self.atoms:
            if atom.label == str(label):
                return atom
        return None

    def select_by_type_and_coord_number(self, atom_type, coord_number):
        """Selects atoms with given atom_type and coordination number"""
        return tuple([atom for atom in self.atoms
                      if atom.type == atom_type and atom.coordination_number == coord_number])

    def filter_by_type_and_coord_number(self, atom_type, coord_number):
        """Selects atoms with given atom_type and coordination number"""
        atoms = list([atom for atom in self.atoms
                      if atom.type.lower() == atom_type.lower() and atom.coordination_number == coord_number])
        return CrystalData(self.filename, self.unit_cell, self.struct_id, self._bond_specs, atoms, self.cell_volume,
                           self.print_header)

    def select_by_label(self, label):
        """Selects one atom with a given specific label!"""
        return next((atom for atom in self.atoms if atom.label.lower() == label.lower()))

    def filter_by_label(self, label):
        """Selects one atom with a given specific label!"""
        atoms = [atom for atom in self.atoms if atom.label.lower() == label.lower()]
        return CrystalData(self.filename, self.unit_cell, self.struct_id, self._bond_specs, atoms, self.cell_volume,
                           self.print_header)

    def filter_by_type(self, atom_type):
        """Selects one atom with a given specific label!"""
        atoms = [atom for atom in self.atoms if atom.type.lower() == atom_type.lower()]
        return CrystalData(self.filename, self.unit_cell, self.struct_id, self._bond_specs, atoms, self.cell_volume,
                           self.print_header)

    def select_by_coordination_number(self, coord_number):
        """Selects the atoms with a given coordination_number!"""
        return tuple([atom for atom in self.atoms if atom.coordination_number == coord_number])

    def filter_by_coordination_number(self, coord_number):
        """Selects atoms with given atom_type and coordination number"""
        atoms = list([atom for atom in self.atoms
                      if atom.coordination_number == coord_number])
        return CrystalData(self.filename, self.unit_cell, self.struct_id, self._bond_specs, atoms, self.cell_volume,
                           self.print_header)

    def select_by_coordinating_atom_type_and_coordination_number(self, atom_type, coord_number):
        """Selects atoms with given atom_type and coordination number"""
        atoms = []
        for atom in self.atoms:
            for coord_atom in atom.coordinating_atoms:
                if coord_atom.atom.type.lower() == atom_type.lower() and coord_atom.atom.coordination_number == coord_number:
                    atoms.append(atom)
        return atoms

    def filter_by_coordinating_atom_type_and_coordination_number(self, atom_type, coord_number):
        """Selects atoms with given atom_type and coordination number"""
        atoms = []
        for atom in self.atoms:
            for coord_atom in atom.coordinating_atoms:
                if coord_atom.atom.type.lower() == atom_type.lower() and coord_atom.atom.coordination_number == coord_number:
                    atoms.append(atom)
        return CrystalData(self.filename, self.unit_cell, self.struct_id, self._bond_specs, atoms, self.cell_volume,
                           self.print_header)

    def filter_by_number_of_coordinating_atom_with_same_type(self, atom_type, number):
        """Selects atoms with given atom_type and coordination number"""
        atoms = []
        counter = 0
        for atom in self.atoms:
            for coord_atom in atom.coordinating_atoms:
                if coord_atom.atom.type.lower() == atom_type.lower():
                    counter += 1
            if counter == number:
                atoms.append(atom)
        return CrystalData(self.filename, self.unit_cell, self.struct_id, self._bond_specs, atoms, self.cell_volume,
                           self.print_header)

    def filter_by_coordinating_atom_distance_between(self, atom_type, lower_bound=0.0, upper_bound=1.2):
        """Selects atoms with given atom_type and coordination number"""
        atoms = []
        counter = 0
        for atom in self.atoms:
            for coord_atom in atom.coordinating_atoms:
                if coord_atom.atom.type.lower() == atom_type.lower() \
                        and coord_atom.distance < upper_bound \
                        and coord_atom.distance > lower_bound:
                    atoms.append(atom)
        return CrystalData(self.filename, self.unit_cell, self.struct_id, self._bond_specs, atoms, self.cell_volume,
                           self.print_header)

    def filter_by_exact_atom_type_coord_number_distance(self, atom_type,tpl_of_atoms):
        """Selects atoms with given atom_type and coordination number"""
        atoms = []
        for atom in self.atoms:
            if atom.type == atom_type:
                counter = 0
                coord_atoms = copy.copy(atom.coordinating_atoms)
                for coord_type, coord_number, bond_name in tpl_of_atoms:
                    for coord_atom in coord_atoms:
                        if coord_atom.atom.type.lower() == coord_type.lower() \
                                and coord_atom.atom.coordination_number == coord_number \
                                and coord_atom.spec_name.lower() == bond_name.lower():
                            counter+=1
                            coord_atoms.remove(coord_atom)
                            break
                if counter == len(tpl_of_atoms):
                    atoms.append(atom)

        return CrystalData(self.filename, self.unit_cell, self.struct_id, self._bond_specs, atoms, self.cell_volume,
                           self.print_header)


    def get_by_coordinating_atom_type_and_coordination_number(self, atom_type, coord_number):
        """Selects atoms with given atom_type and coordination number"""
        atoms = []
        for atom in self.atoms:
            for coord_atom in atom.coordinating_atoms:
                if coord_atom.atom.type.lower() == atom_type.lower() and coord_atom.atom.coordination_number == coord_number:
                    atoms.append(coord_atom.atom)
        return CrystalData(self.filename, self.unit_cell, self.struct_id, self._bond_specs, atoms, self.cell_volume,
                           self.print_header)

    def set_group_number(self, number, atoms=None):

        if atoms is None:
            atoms = self.atoms
        for atom in atoms:
            atom.group = number

    def set_group_repr(self, repr, atoms=None):

        if atoms is None:
            atoms = self.atoms
        for atom in atoms:
            atom.group_repr = repr

    def set_representation(self, label=True, filename=True, type=False, iso=True, cq=True, eta=True, coord_number=True,
                           group=True,
                           group_repr=True, u=False, v=False, w=False, x=False, y=False, z=False, aniso=False,
                           asym=False, header=False):
        if header:
            self.print_header = True
        if all([isinstance(item, bool) for item in
                (label, filename, type, iso, cq, eta, coord_number, group, group_repr, u, v, w, x, y, z, aniso, asym)]):
            for atom in self.atoms:
                atom.is_printable = PrintableParam(label, filename, type, iso, cq, eta, coord_number, group, group_repr,
                                                   u, v, w, x, y, z, aniso, asym)
        else:
            print('All arguments should be either True or False!')

    def add_bond_specification(self, atom_type1, atom_type2, lower_bound_length=0.0, upper_bound_length=2.2,
                               spec_name=None):

        if not spec_name:
            spec_name = len(self._bond_specs) + 1
        self._bond_specs.append(BondSpec(spec_name, atom_type1, atom_type2, lower_bound_length, upper_bound_length))

    def remove_bond_specification(self, spec_name):
        to_be_removed = None
        for spec in self._bond_specs:
            if spec_name == spec.spec_name:
                to_be_removed = spec
        if to_be_removed:
            self._bond_specs.remove(spec_name)
        else:
            print("Couldn't find bond specification named: {0}".format(spec_name))

    def write_pdb(self, filename):
        fmt = "{:6s}{:5d} {:^4s}{:1s}{:3s} {:1s}{:4d}{:1s}   {:8.3f}{:8.3f}{:8.3f}\n"
        with open(filename, 'w') as file:
            counter = 1
            for atom in self.atoms:
                file.write(fmt.format('ATOM', counter, atom.atom_type, "", 'MOL', 'A', 1, "", atom.x, atom.y, atom.z))
                counter += 1
            file.write('END\n')

    def show(self, atoms=None):
        if not atoms:
            atoms = self.atoms

        if self.print_header:
            headers = []
            try:
                fmt = atoms[0]._format(header=True)
            except:
                return print('No atom with the specified criteria in file {} !'.format(self.filename))
            for hdr, is_printable in zip(atoms[0].is_printable._fields, atoms[0].is_printable):
                if is_printable:
                    headers.append(hdr)
            print(fmt.format(*headers))

        for atom in atoms:
            print(atom.repr)

    def show_header(self):

        headers = []
        try:
            fmt = self.atoms[0]._format(header=True)
        except:
            print("Couldn't print header, there are no atoms in the structure!")
            return
        for hdr, is_printable in zip(self.atoms[0].is_printable._fields, self.atoms[0].is_printable):
            if is_printable:
                headers.append(hdr)
        print(fmt.format(*headers))

    def write_header(self, filename='output.txt', write_option='w'):

        with open(filename, write_option) as file:

            headers = []
            try:
                fmt = self.atoms[0]._format(header=True)
            except:
                print("Couldn't write header, there are no atoms in the structure!")
                return None
            for hdr, is_printable in zip(self.atoms[0].is_printable._fields, self.atoms[0].is_printable):
                if is_printable:
                    headers.append(hdr)
            file.write(fmt.format(*headers) + '\n')

    def write_to_file(self, filename = 'output.txt', atoms=None, write_option='a', header=False):

        if not atoms:
            atoms = self.atoms

        with open(filename, write_option) as file:

            for atom in atoms:
                file.write(atom.repr + '\n')
