     @property
    def x(self):
        return calculate_x_cart(self.unit_cell, self.u, self.v, self.w)

    @property
    def y(self):
        return  calculate_y_cart(self.unit_cell, self.v, self.w)

    @property
    def z(self):
        return calculate_z_cart(self.unit_cell, self.w)

    @property
    def _x_plus(self):
        return calculate_x_cart(self.unit_cell, self.u+1, self.v, self.w)

    @property
    def _y_plus(self):
        return calculate_y_cart(self.unit_cell, self.v+1, self.w)

    @property
    def _z_plus(self):
        return calculate_z_cart(self.unit_cell, self.w+1)


    @property
    def _x_minus(self):
        return calculate_x_cart(self.unit_cell, self.u-1, self.v, self.w)

    @property
    def _y_minus(self):
        return calculate_y_cart(self.unit_cell, self.v-1, self.w)

    @property
    def _z_minus(self):
        return calculate_z_cart(self.unit_cell, self.w-1)


            def read_coordination(self, filename):
        """Sets the coordinating atoms of each atom of the CrystalData instance by the parsing of
        the visible_coordination file!"""
        with open(filename, 'r') as file:
            # Locating the '----' line, and skipping the next three rows!
            while True:
                line = next(file)
                if '----' in line:
                    for _ in range(3):
                        next(file)
                    break
            try:
                while True:
                    # Getting the first line and select the atom as target atom for setting the coordinations!
                    line = next(file)
                    target_atom = self.get_atom(label_crystal_coord_atom(line))
                    if target_atom:
                        while True:
                            # Iterating over the lines until an empty line is found!
                            line = next(file)
                            if line.strip():
                                # Getting the label and distance of the coordinating atom!
                                coord_atom = self.get_atom(label_visible_coord_atom(line))
                                distance = distance_visible_coord_atom(line)
                                # If the coordinating atom is already presented in the target atom's coordinating atom list,
                                # then skip, otherwise put it in the list!
                                if coord_atom not in [atom.atom for atom in target_atom.coordinating_atoms]:
                                    target_atom.coordinating_atoms.append(CoordAtom(coord_atom, distance))
                            else:
                                break
                    else:
                        break
            except StopIteration:
                pass


   filtered_data = struct.filter_by_type_and_coord_number('O', 3)
    filtered_data = filtered_data.filter_by_coordinating_atom_type_and_coordination_number('H', 1)
    filtered_data = filtered_data.filter_by_number_of_coordinating_atom_with_same_type('H', 2)
    filtered_data = filtered_data.filter_by_coordinating_atom_distance_between('H', 0, 1.2)
    filtered_data = filtered_data.filter_by_coordinating_atom_distance_between('H', 1.2, 1.9)
    filtered_data = filtered_data.filter_by_coordinating_atom_type_and_coordination_number('Al', 4)
    for atom in filtered_data:
        atom.group = 4
        atom.group_repr = 'Al(IV)_O(III)_Hi_Hk'
    filtered_data.write_to_file('output.txt')
