from collections import OrderedDict

    # if nothing is given the script will read for all files with .castep extension, if a filename is given it will
    # read that specific file, if a list is given, it will read the filenames in the given list.
    #IMPORTANT: do not set extension!

    #filenames = 'sc3_110s10'
    #filenames = ['sc3_110s10','sc3_110s7_w10']
filenames = ''


    #Definitions of bonds for coordination number calculation and data filtering! At least one bond needs
    #to be defined!
    # type1 type2 lower_boundary upper_boundary bond_name
bond_specifications = [
    ('Al', 'O', 0.0, 2.2, 'AL_O'),
    ('O', 'H', 0.0, 1.2, 'OH_cov'),
    ('O', 'H', 1.2, 1.9, 'OH_ion')
]

    #Setting up printing and file writing options! Do not change the given order, only the values (True/False)!
rows_to_print = OrderedDict()
rows_to_print['labels'] = True
rows_to_print['filename'] = True
rows_to_print['type'] = False
rows_to_print['iso'] = True
rows_to_print['cq'] = True
rows_to_print['eta'] = True
rows_to_print['coord'] = True
rows_to_print['group'] = True
rows_to_print['g_repr'] = True
rows_to_print['u'] = False
rows_to_print['v'] = False
rows_to_print['w'] = False
rows_to_print['x'] = False
rows_to_print['y'] = False
rows_to_print['z'] = False
rows_to_print['aniso'] = False
rows_to_print['asym'] = False

#Filter functions:

# .filter_by_label(label)                                                                                   --> Returns CrystalData strucutre of atoms with a given label!
# .filter_by_type(atom_type)                                                                                --> Returns CrystalData strucutre of atoms with a given type!
# .filter_by_coordination_number(coord_number)                                                              --> Returns CrystalData strucutre of atoms with a given coordination_number!
# .filter_by_type_and_coord_number(atom_type, coord_number)                                                 --> Returns CrystalData strucutre of atoms with a given type and coordination number!
# .filter_by_coordinating_atom_type_and_coordination_number(atom_type, coord_number)                        --> Returns CrystalData strucutre of atoms based on coordinating atom type and coordinating atom number
# .filter_by_number_of_coordinating_atom_with_same_type(atom_type, number)                                  --> Returns CrystalData strucutre of atoms where there are a given number of coordinating atoms with the same type
# .filter_by_coordinating_atom_distance_between(atom_type, lower_bound=0.0, upper_bound=1.2)                --> Returns CrystalData strucutre of atoms based on the coordinating atom's type and distance
# .filter_by_exact_atom_type_coord_number_distance(atom_type,tpl_of_atoms)                                  --> Most generic filter function so far. It returns a CrystalData structure of atoms with a given atom type,
#                                                                                                                and given coordinating atom type, coordination number and bond type! The coordinating atoms shoud be given as a tuple
#                                                                                                                in the following way: (coordinating_atom_type, coordinating_atom_coord_number,bond_name).
#                                                                                                                bond name should be one of the names predefined in the config!