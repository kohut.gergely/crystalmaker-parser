import glob

import config
from readers import castep_reader


def configurate_structures(files=config.filenames, extention='castep', calc_coordination=True,
                           rows_to_print=config.rows_to_print):
    out = []
    if not files:
        files = [file for file in glob.glob('*.' + extention)]
    if isinstance(files, str):
        files = [files]
    data = list(filter(lambda x: bool(x), [castep_reader(filename, counter) for counter, filename in enumerate(files)]))
    if not data:
        raise ValueError('Nothing to configure!')
    else:
        for index, struct in enumerate(data):
            print('Configuring {}...'.format(struct))
            for spec in config.bond_specifications:
                struct.add_bond_specification(*spec)
                struct.set_representation(*rows_to_print.values())
            if calc_coordination:
                struct.calculate_coordination()
            out.append(struct)
        return out
