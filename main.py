from main_functions import configurate_structures

#Preprocessing the files using the configure_structures function! It reads every file into a separate data structure,
#adds bond specifications to them, sets their representations and calculates the coordination numbers!
configured_data = configurate_structures()

#Writes the header to the file using the first structure's write_header() functiion!. If no input is given to write_header(), the
# default filename is 'output.txt'! Default is overwriting!
configured_data[0].write_header()

#print the header into the standard output!
configured_data[0].show_header()

#Iterating over every structure! Each structure represents one preprocessed file.
for struct in configured_data:

    #Applying filtering functions on each structure using filtering functions! Every structure which starts with "filter" in its name
    # will filter out atoms based on the criteria. For more information about the available filter functions, see the config file!

    #filtering O atoms which coordinates with an Al of coord number=5, an Al of coord number=4, and an H with covalent bond!
    filtered_data = struct.filter_by_exact_atom_type_coord_number_distance('O',(('Al',4,'Al_O'),('Al',4,'Al_O'),('H',1,'OH_cov')))
    #printing it on the screen
    filtered_data.show()
    #setting up the group number and group description:
    filtered_data.set_group_number(2)
    filtered_data.set_group_repr('Al(IV)Al(IV)OHc')
    #writing to the output file!
    filtered_data.write_to_file()

for struct in configured_data:
    filtered_data = struct.filter_by_type_and_coord_number('O',3)
    filtered_data = filtered_data.filter_by_exact_atom_type_coord_number_distance('O', (('Al', 4, 'Al_O'), ('Al', 5, 'Al_O'), ('H', 1, 'OH_cov')))
    filtered_data.set_group_number(3)
    filtered_data.set_group_repr('Al(IV)Al(V)OHc')
    filtered_data.show()
    filtered_data.write_to_file()

for struct in configured_data:
    filtered_data = struct.filter_by_type_and_coord_number('O', 3)
    filtered_data = filtered_data.filter_by_exact_atom_type_coord_number_distance('O', (('Al', 4, 'Al_O'), ('Al', 6, 'Al_O'), ('H', 1, 'OH_cov')))
    filtered_data.set_group_number(4)
    filtered_data.set_group_repr('Al(IV)Al(VI)OHc')
    filtered_data.show()
    filtered_data.write_to_file()

for struct in configured_data:
    filtered_data = struct.filter_by_type_and_coord_number('O', 3)
    filtered_data = filtered_data.filter_by_exact_atom_type_coord_number_distance('O', (('Al', 5, 'Al_O'), ('Al', 5, 'Al_O'), ('H', 1, 'OH_cov')))
    filtered_data.set_group_number(5)
    filtered_data.set_group_repr('Al(V)Al(V)OHc')
    filtered_data.show()
    filtered_data.write_to_file()

for struct in configured_data:
    filtered_data = struct.filter_by_type_and_coord_number('O', 3)
    filtered_data = filtered_data.filter_by_exact_atom_type_coord_number_distance('O', (('Al', 5, 'Al_O'), ('Al', 6, 'Al_O'), ('H', 1, 'OH_cov')))
    filtered_data.set_group_number(6)
    filtered_data.set_group_repr('Al(V)Al(VI)OHc')
    filtered_data.show()
    filtered_data.write_to_file()

for struct in configured_data:
    filtered_data = struct.filter_by_type_and_coord_number('O', 3)
    filtered_data = filtered_data.filter_by_exact_atom_type_coord_number_distance('O', (('Al', 6, 'Al_O'), ('Al', 6, 'Al_O'), ('H', 1, 'OH_cov')))
    filtered_data.set_group_number(7)
    filtered_data.set_group_repr('Al(VI)Al(VI)OHc')
    filtered_data.show()
    filtered_data.write_to_file()

for struct in configured_data:
    filtered_data = struct.filter_by_type_and_coord_number('O', 3)
    filtered_data = filtered_data.filter_by_exact_atom_type_coord_number_distance('O', (('H', 1, 'OH_ion'),('H', 1, 'OH_cov'),('Al', 4, 'Al_O')))
    filtered_data.set_group_number(8)
    filtered_data.set_group_repr('Al(IV)OHcHi')
    filtered_data.show()
    filtered_data.write_to_file()

for struct in configured_data:
    filtered_data = struct.filter_by_type_and_coord_number('O', 3)
    filtered_data = filtered_data.filter_by_exact_atom_type_coord_number_distance('O', (('H', 1, 'OH_ion'),('H', 1, 'OH_cov'),('Al', 5, 'Al_O')))
    filtered_data.set_group_number(9)
    filtered_data.set_group_repr('Al(V)OHcHi')
    filtered_data.show()
    filtered_data.write_to_file()

for struct in configured_data:
    filtered_data = struct.filter_by_type_and_coord_number('O', 3)
    filtered_data = filtered_data.filter_by_exact_atom_type_coord_number_distance('O', (('H', 1, 'OH_ion'),('H', 1, 'OH_cov'),('Al', 6, 'Al_O')))
    filtered_data.set_group_number(10)
    filtered_data.set_group_repr('Al(VI)OHcHi')
    filtered_data.show()
    filtered_data.write_to_file()


for struct in configured_data:
    filtered_data = struct.filter_by_type_and_coord_number('O', 3)
    filtered_data = filtered_data.filter_by_exact_atom_type_coord_number_distance('O', (('H', 1, 'OH_cov'),('H', 1, 'OH_cov'),('Al', 4, 'Al_O')))
    filtered_data.set_group_number(11)
    filtered_data.set_group_repr('Al(IV)OHcHc')
    filtered_data.show()
    filtered_data.write_to_file()

for struct in configured_data:
    filtered_data = struct.filter_by_type_and_coord_number('O', 3)
    filtered_data = filtered_data.filter_by_exact_atom_type_coord_number_distance('O', (('H', 1, 'OH_cov'),('H', 1, 'OH_cov'),('Al', 5, 'Al_O')))
    filtered_data.set_group_number(12)
    filtered_data.set_group_repr('Al(V)OHcHc')
    filtered_data.show()
    filtered_data.write_to_file()

for struct in configured_data:
    filtered_data = struct.filter_by_type_and_coord_number('O', 3)
    filtered_data = filtered_data.filter_by_exact_atom_type_coord_number_distance('O', (('H', 1, 'OH_cov'),('H', 1, 'OH_cov'),('Al', 6, 'Al_O')))
    filtered_data.set_group_number(13)
    filtered_data.set_group_repr('Al(VI)OHcHc')
    filtered_data.show()
    filtered_data.write_to_file()

for struct in configured_data:
    filtered_data = struct.filter_by_type_and_coord_number('O', 3)
    filtered_data = filtered_data.filter_by_exact_atom_type_coord_number_distance('O', (('H', 1, 'OH_cov'),('H', 1, 'OH_cov'),('H', 1, 'OH_ion')))
    filtered_data.set_group_number(14)
    filtered_data.set_group_repr('OHcHcHi')
    filtered_data.show()
    filtered_data.write_to_file()