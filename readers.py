from reader_functions import *
from structure.structures import *


def crystal_coordination_reader(filename, struct_id=1):
    """Reads a CrystalMaker type crystal coordination file and creates a data structure"""

    cell_param_read = False
    try:

        with open(filename, 'r') as file:

            while True:
                # Locating the line which contains the 'Unit cell parameters:' string!'
                line = next(file)
                if 'Unit cell parameters:' in line.strip():
                    # Getting the line with box edges and box angles
                    box_edges = next(file).strip().split()
                    box_angles = next(file).strip().split()
                    try:
                        # parisng the lines and store the parameters in a corresponding UnitCell namedtuple
                        unit_cell = UnitCell(float(box_edges[2].strip(';')), float(box_edges[5].strip(';')),
                                             float(box_edges[8].strip(';')),
                                             float(box_angles[2].strip(';')), float(box_angles[5].strip(';')),
                                             float(box_angles[8].strip(';')))
                        # if everything went fine, set cell_param_read boolean to True
                        cell_param_read = True
                    except:
                        raise TypeError("Couldn't read the box parameters properly!")

                # If everything went well gathering the cell parameters and bond specifications, collecting the atomic information
                # as a CrystalAtom object and storing everything in a CrystalData structure (see structures module for details!)
                if cell_param_read:
                    crystal_data = CrystalData(filename, unit_cell, struct_id)
                    for row in file:
                        if row.strip().startswith('['):
                            crystal_data.atoms.append(CastepAtom(*parse_crystal_coord_atom(row), unit_cell=unit_cell,
                                                                 cell_volume=crystal_data.cell_volume))
                    break

            return crystal_data
    except FileNotFoundError:
        print("Couldn't find {}".format(filename))


def castep_reader(filename, struct_id=1):
    coordinates_data_indicator = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
    chemical_shift_data_indicator = 'Chemical Shielding and Electric Field Gradient Tensors'
    crystal_data = None
    try:
        with open(filename, 'r') as file:

            while True:
                # Locating the line which contains the 'Unit cell parameters:' string!'
                line = next(file)
                if 'Lattice parameters(A)' in line.strip():
                    # Getting the line with box edges and box angles
                    box_param_line1 = next(file).strip().split()
                    box_param_line2 = next(file).strip().split()
                    box_param_line3 = next(file).strip().split()
                    try:
                        # parisng the lines and store the parameters in a corresponding UnitCell namedtuple
                        unit_cell = UnitCell(float(box_param_line1[2].strip()), float(box_param_line2[2].strip()),
                                             float(box_param_line3[2].strip()),
                                             float(box_param_line1[5].strip()), float(box_param_line2[5].strip()),
                                             float(box_param_line3[5].strip()))
                        # if everything went fine, set cell_param_read boolean to True
                    except:
                        raise TypeError("Couldn't read the box parameters properly!")

                if coordinates_data_indicator in line.strip():
                    crystal_data = CrystalData(filename, unit_cell, struct_id)
                    for _ in range(3):
                        next(file)
                    while True:
                        line = next(file)
                        if coordinates_data_indicator in line:
                            break
                        else:
                            new_atom = CastepAtom(crystal_data.filename, *parse_casep_atom(line), unit_cell=unit_cell,
                                                  cell_volume=crystal_data.cell_volume)
                            crystal_data.atoms.append(new_atom)

                if chemical_shift_data_indicator in line.strip():
                    for _ in range(3):
                        next(file)
                    while True:
                        line = next(file)
                        if '========================================================================' in line:
                            break
                        else:
                            splitline = line.strip().split()
                            label = splitline[1] + splitline[2]
                            target_atom = crystal_data.select_by_label(label)
                            parse_casep_atom_chemical_information(target_atom, line)

                    break
    except FileNotFoundError:
        print("Couldn't find {}".format(filename))
    finally:
        return crystal_data
